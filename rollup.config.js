import typescript from "rollup-plugin-typescript2";
import pkg from "./package.json";
import { terser } from "rollup-plugin-terser";
import resolver from "rollup-plugin-node-resolve";
import commonJS from "rollup-plugin-commonjs";
import peerDepsExternal from "rollup-plugin-peer-deps-external";

export default {
  input: "src/index.tsx",
  output: [
    {
      file: pkg.main,
      format: "cjs",
      sourcemap: true,
    },
    {
      file: pkg.module,
      format: "es",
      sourcemap: true,
    },
  ],
  external: [...Object.keys(pkg.dependencies || {})],
  plugins: [
    peerDepsExternal(),
    resolver({ browser: true }),
    typescript({
      typescript: require("typescript"),
    }),
    commonJS({
      include: "node_modules/**",
    }),
    terser(),
  ],
};
