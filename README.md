# React Styled Component Calendar

## Installation

```bash
yarn add react-styled-component-calendar
```

or

```bash
npm install --save react-styled-component-calendar
```

## Usage

```js
import Calendar from 'react-styled-component-calendar'

const events = [{
    "title": "Two Volcano Sprint",
    "startDate": "2020-10-18T05:30+01:00",
    "endDate": "2020-10-24T00:00+00:00"
  }, {
    "title": "Silk Road Mountain Race",
    "startDate": "2020-08-14T00:00+00:00",
    "endDate": "2020-08-21T00:00+00:00"
  }, {
    "title": "Transcontinental No.8",
    "startDate": "2020-07-25T00:00+00:00",
    "endDate": "2020-08-01T00:00+00:00"
  }, {
    "title": "TransIbérica",
    "startDate": "2020-07-12T22:00+01:00",
    "endDate": "2020-07-26T22:00+01:00"
  }, {
    "title": "Transpyrenees",
    "startDate": "2020-06-27T21:00+01:00",
		"endDate": "2020-07-03T21:00+01:00"
	}, {
    "title": "GBDURO20",
    "startDate": "2020-06-27T08:00+00:00",
    "endDate": "2020-07-04T00:00+00:00"
  }, {
    "title": "Tour Divide",
    "startDate": "2020-06-12T08:00-07:00",
    "endDate": "2020-07-10T00:00+00:00"
  }, {
    "title": "Trans America Bike Race",
    "startDate": "2020-06-07T06:00-08:00",
		"endDate": "2020-06-20T18:00-05:00"
	}
}]

<Calendar events={events}>
```

### Props

| **prop**                       | **type** | **required** | **description**                                                                                                                                                |
| ------------------------------ | -------- | ------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| events                         | array    | \*           | array of events to pass to the calendar, so below for format of event                                                                                          |
| dateInView                     | string   |              | the current date to be displayed. JavaScript new Date() object. n.b month index starts at 0                                                                    |
| navMonthFormat                 | string   |              | moment js month format                                                                                                                                         |
| navYearsBeforeNow              | number   |              | number of years to show before current year                                                                                                                    |
| navYearsAfterNow               | number   |              | number of years to show before current year                                                                                                                    |
| outOfMonthCalendarDayHighlight | string   |              | cell hightlight for days outside of visible month                                                                                                              |
| todayColorHightight            | string   |              | cell highlight for today                                                                                                                                       |
| onPrevMonthClick               | function |              | (month: number) => {} A function that is fired after month change containing the new month                                                                     |
| onNextMonthClick               | function |              | (month: number) => {} A function that is fired after month change containing the new month                                                                     |
| CalendarCellRenderer           | function |              | ({ index: number, events:Event[] }): React.FC<{}> => null; Full override function that returns a Calendar Cell Component. All other Cell props will be ignored |
| MonthNavRenderer               | function |              | (): React.FC<{}> => {} Function that returns the month nav component                                                                                           |
| WeekHeaderRenderer             | function |              | ({ days: string[] }}): React.FC<{}> => {} Function that returns the week nav component                                                                         |

### Event

| **attribute** | **required** | **type** | **description**                                                  |
| ------------- | ------------ | -------- | ---------------------------------------------------------------- |
| title         | \*           | string   | Title to render for given range                                  |
| startDate     | \*           | DateTime | start date                                                       |
| endDate       | \*           | DateTime |
| ...props      |              | any      | any other values you would like to use in a custom calender cell |
