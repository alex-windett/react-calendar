import React from "react";

import events from "../src/data/events.json";

// @ts-ignore
// import Calendar from "react-styled-component-calendar";
import Calendar from "../../";
// import moment from "moment";

// const CalendarCell = (props: any) => {
//   return (
//     <div style={{ border: "1px solid lightblue" }}>
//       <p>{moment(props.date).format("dddd Do")}</p>
//       <p>In View Month {JSON.stringify(props.isCurrentMonth)}</p>
//       <p>isToday {JSON.stringify(props.isToday)}</p>
//       <p>events:</p>
//       {props.events.map((e: any, i: number) => (
//         <p key={i}>{e.title}</p>
//       ))}
//     </div>
//   );
// };

// const MonthHeader = (props: any) => {
//   const handlePrevClick = (event: any) => {
//     debugger;
//   };

//   return (
//     <div>
//       <button type="button" onClick={handlePrevClick}>
//         prev
//       </button>
//       current Month {moment(props.currentDate).format("MMM")}
//       <button type="button">next</button>
//     </div>
//   );
// };

export default () => {
  //   <Calendar
  //   events={events}
  //   currentDate={currentDate}
  //   setCurrentDate={setCurrentDate}
  //   handleNextMonthClick={handleNextMonthClick}
  //   handlePrevMonthClick={handlePrevMonthClick}
  // />

  return <Calendar events={events} />;
};
