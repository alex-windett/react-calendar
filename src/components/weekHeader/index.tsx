import * as React from "react";
import { eachDayOfInterval } from "date-fns";
import styled from "styled-components";
import moment from "moment";
import { WeekHeaderProps } from "../../types";

const WeekHeader = styled.header`
  display: grid;
  grid-template-columns: repeat(7, 1fr);
  text-align: center;
  grid-auto-rows: 100px;
  align-items: center;

  @media screen and (max-width: 48em) {
    display: none;
  }
`;

const weekDayNames = () => {
  // Set a generic known start and end of week
  // Months are 0 indexed so 11 is Dec
  const startOfWeek = new Date(2017, 11, 4);
  const endOfWeek = new Date(2017, 11, 10);

  const weekArr = eachDayOfInterval({ start: startOfWeek, end: endOfWeek });
  return weekArr.map(d => moment(d).format("ddd"));
};

export default ({ WeekHeaderRenderer }: WeekHeaderProps) => {
  if (WeekHeaderRenderer && typeof WeekHeaderRenderer === "function") {
    return WeekHeaderRenderer({ days: weekDayNames() });
  }

  return (
    <WeekHeader>
      {weekDayNames().map((d, i) => (
        <span key={i}>{d}</span>
      ))}
    </WeekHeader>
  );
};
