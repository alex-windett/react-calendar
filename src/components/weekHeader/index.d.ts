/// <reference types="react" />
import { WeekHeaderProps } from "../../types";
declare const _default: ({ WeekHeaderRenderer }: WeekHeaderProps) => void | JSX.Element;
export default _default;
