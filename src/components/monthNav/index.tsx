import * as React from "react";

import { format } from "date-fns";
import styled from "styled-components";
import moment from "moment";

import { getMonth } from "date-fns";
import { MonthNavProps } from "../../types";

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  text-align: center;
  padding-bottom: 45px;
  padding-top: 45px;
  border-bottom: 1px solid black;
  background-color: transparent;

  @media screen and (max-width: 48em) {
    margin-top: 30px;
    margin-bottom: 30px;
    padding-bottom: 15px;
    padding-top: 30px;
    justify-content: space-between;
    border-bottom: none;
  }
`;

const Placeholder = styled.div`
  min-height: 100%;
  text-align: center;
  margin: 0 30px;
`;

const CalenderChange = styled.button`
  text-decoration: underline;
  appearance: none;
  -webkit-appearance: none;
  border: none;
  cursor: pointer;
  outline: none;
  background-color: transparent;

  img {
    width: 10px;
  }
`;

const YearWrapper = styled.div`
  margin-bottom: 15px;
`;

const MonthTitle = styled.h2`
  margin-bottom: 15px;
`;

const MonthNav = styled.div`
  grid-area: nav;
  display: flex;
  align-items: center;
  justify-content: center;

  @media screen and (max-width: 48em) {
    grid-column: 4 / 6;
    justify-content: space-between;
  }
`;

const Today = styled.div`
  grid-area: today;

  button {
    cursor: pointer;
  }
`;

const StyledSelect = styled.select`
  border: 2px solid black;
  min-width: 200px;
  max-width: 200px;

  & + & {
    @media screen and (max-width: 48em) {
      margin-top: 30px;
    }

    @media screen and (min-width: 48em) {
      margin-left: 30px;
    }
  }
`;

const DateFilter = styled.div`
  margin-top: 30px;
`;

const getYears = ({
  currentYear,
  yearsBefore,
  yearsAfter
}: {
  currentYear: number;
  yearsBefore: number;
  yearsAfter: number;
}) => {
  let years = [];
  for (let i = currentYear; i > currentYear - yearsBefore; i--) {
    years.push(i);
  }

  for (let i = currentYear; i < currentYear + yearsAfter; i++) {
    years.push(i);
  }

  years = [...new Set(years)];
  years.sort((a, b) => a - b);

  return years;
};

const Nav = ({
  handleNextMonthClick,
  handlePrevMonthClick,
  currentDate,
  setCurrentDate,
  navMonthFormat = "MMM",
  navYearsBeforeNow = 5,
  navYearsAfterNow = 5
}: MonthNavProps) => {
  const month = format(currentDate, navMonthFormat);
  const year = format(currentDate, "yyyy");
  const intYear = parseInt(year);
  const intMonth = getMonth(currentDate);

  const monthRef = React.useRef(null);
  const yearRef = React.useRef(null);

  const years = getYears({
    currentYear: intYear,
    yearsBefore: navYearsAfterNow,
    yearsAfter: navYearsBeforeNow
  });

  const handleChange = () => {
    const { current: month } = monthRef;
    const { current: year } = yearRef;

    // @ts-ignore
    const date = new Date(year.value, month.value);

    setCurrentDate(date);
  };

  const handleTodayClick = () => {
    setCurrentDate(Date.now());
  };

  return (
    <Wrapper>
      <Today>
        <button type="button" onClick={handleTodayClick}>
          Today
        </button>
      </Today>
      <MonthNav>
        <Placeholder>
          <CalenderChange onClick={handlePrevMonthClick}>Prev</CalenderChange>
        </Placeholder>

        <YearWrapper>
          <MonthTitle>{month}</MonthTitle>
          <span>{year}</span>
        </YearWrapper>

        <Placeholder>
          <CalenderChange onClick={handleNextMonthClick}>Next</CalenderChange>
        </Placeholder>
      </MonthNav>

      <DateFilter>
        <StyledSelect
          onChange={handleChange}
          name="year"
          ref={yearRef}
          defaultValue={years.find(y => y === intYear)}
        >
          {years.map(y => (
            <option key={y} value={y}>
              {y}
            </option>
          ))}
        </StyledSelect>

        <StyledSelect
          onChange={handleChange}
          name="month"
          ref={monthRef}
          defaultValue={[...Array(12).keys()].find(m => m === intMonth)}
        >
          {[...Array(12).keys()].map(m => (
            <option key={m} value={m}>
              {moment(m + 1, "MM").format(navMonthFormat)}
            </option>
          ))}
        </StyledSelect>
      </DateFilter>
    </Wrapper>
  );
};

export default Nav;
