/// <reference types="react" />
import { MonthNavProps } from "../../types";
declare const Nav: ({ handleNextMonthClick, handlePrevMonthClick, currentDate, setCurrentDate, navMonthFormat, navYearsBeforeNow, navYearsAfterNow }: MonthNavProps) => JSX.Element;
export default Nav;
