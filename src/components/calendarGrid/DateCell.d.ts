/// <reference types="react" />
import { DateCellProps } from "../../types";
export declare const Cell: import("styled-components").StyledComponent<"div", any, {
    isCurrentMonth: boolean;
    isToday: boolean;
    index: number;
    todayColorHighlight?: string | undefined;
    outOfMonthColorHighlight?: string | undefined;
}, never>;
declare const DateCell: ({ isCurrentMonth, events, date, index, todayColorHighlight, outOfMonthColorHighlight }: DateCellProps) => JSX.Element;
export default DateCell;
