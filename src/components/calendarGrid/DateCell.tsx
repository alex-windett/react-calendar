import * as React from "react";
import styled from "styled-components";
import moment from "moment";

import { DateCellProps } from "../../types";
import EventCell from "./EventCell";

const DayOfWeek = styled.span`
  @media screen and (min-width: 48em) {
    display: none;
  }
`;

export const Cell = styled.div(
  (props: {
    isCurrentMonth: boolean;
    isToday: boolean;
    index: number;
    todayColorHighlight?: string;
    outOfMonthColorHighlight?: string;
  }) => `

	min-height: 150px;
	border-bottom: 1px solid black;
	padding: 15px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;

	${!props.isCurrentMonth ? `background-color: ${props.outOfMonthColorHighlight};` : ""}
	${props.isToday ? `background-color: ${props.todayColorHighlight};` : ""}

	@media screen and (min-width: 48em) {
		${props.index % 7 !== 0 && "border-left: 1px solid black;"}

		&:nth-last-child(-n+7) {
			border-bottom: none;
		}
	}
`
);

const StyledDate = styled.div`
  order: 5;
`;

const StyledEvents = styled.div`
  grid-area: event;
`;

const DateCell = ({
  isCurrentMonth,
  events,
  date,
  index,
  todayColorHighlight,
  outOfMonthColorHighlight
}: DateCellProps) => {
  const dateNo = moment(date).format("Do");
  const dayOfWeek = moment(date).format("ddd");

  const m_today = moment(Date.now());
  const m_date = moment(date);
  const isToday = m_date.startOf("day").isSame(m_today.startOf("day"));

  return (
    <Cell
      isCurrentMonth={isCurrentMonth}
      isToday={isToday}
      index={index}
      todayColorHighlight={todayColorHighlight}
      outOfMonthColorHighlight={outOfMonthColorHighlight}
    >
      <StyledDate>
        <DayOfWeek>{dayOfWeek} &nbsp;</DayOfWeek>
        <span>{dateNo}</span>
      </StyledDate>

      <StyledEvents>
        {events.map((e, i) => (
          <EventCell key={i} calendarDate={date} dayIndex={index} {...e} />
        ))}
      </StyledEvents>
    </Cell>
  );
};

export default DateCell;
