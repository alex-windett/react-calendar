import * as React from "react";
import { CalendarEvent, CalendarGridProps } from "../../types";

import {
  startOfMonth,
  endOfMonth,
  startOfWeek,
  endOfWeek,
  eachDayOfInterval,
  isSameMonth,
  isWithinInterval,
  isToday
} from "date-fns";
import { DateTime } from "luxon";

import styled from "styled-components";

import DateCell from "./DateCell";

const CalendarLayout = styled.article`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-auto-rows: 1fr;
  border: 1px solid black;

  @media screen and (min-width: 48em) {
    grid-template-columns: repeat(7, 1fr);
  }
`;

const daysOfMonth = (date: Date | number) => {
  const currentDate = date;

  const startMonth = startOfMonth(date);
  const endMonth = endOfMonth(date);

  const startCalMonth = startOfWeek(startMonth, { weekStartsOn: 1 });
  const endCalMonth = endOfWeek(endMonth, { weekStartsOn: 1 });

  const dayArray = eachDayOfInterval({ start: startCalMonth, end: endCalMonth });

  return dayArray.map(date => ({
    date: date,
    isCurrentMonth: isSameMonth(currentDate, date),
    isToday: isToday(date),
    events: []
  }));
};

// Only needed until date-fns release timezone support
const removeTimeZoneFromDate = (date: string) => {
  const parsedDate = DateTime.fromISO(date, { setZone: true });

  return DateTime.local(parsedDate.year, parsedDate.month, parsedDate.day).toJSDate();
};

const daysAndEvents = (events: CalendarEvent[] = [], currentDate: Date | number) => {
  const monthDays = daysOfMonth(currentDate);

  const tzFreeEvents = events.map((e: CalendarEvent) => {
    return {
      ...e,
      start: removeTimeZoneFromDate(e.startDate),
      end: removeTimeZoneFromDate(e.endDate)
    };
  });

  return monthDays.map((d: any) => {
    const dayEvents = tzFreeEvents.filter(event =>
      isWithinInterval(d.date, {
        start: event.start,
        end: event.end
      })
    );

    // Order events of current day in start date order
    const sortedEvents = dayEvents.slice().sort((a: any, b: any) => b.end - a.end);

    return {
      ...d,
      events: sortedEvents
    };
  });
};

const Layout = ({
  events,
  currentDate,
  CalendarCellRenderer,
  todayColorHighlight,
  outOfMonthColorHighlight
}: CalendarGridProps) => {
  const _events = daysAndEvents(events, currentDate);

  const Cell =
    CalendarCellRenderer && typeof CalendarCellRenderer === "function"
      ? CalendarCellRenderer
      : DateCell;

  return (
    <CalendarLayout>
      {_events.map((d: CalendarEvent, i: number) => (
        // @ts-ignore
        <Cell
          todayColorHighlight={todayColorHighlight}
          outOfMonthColorHighlight={outOfMonthColorHighlight}
          key={i}
          {...d}
          index={i}
        />
      ))}
    </CalendarLayout>
  );
};

export default Layout;
