import * as React from "react";
import styled from "styled-components";
import { EventCellProps } from "../../types";

const EventCell = styled.div`
  position: relative;
  padding: 5px;
  border-radius: 4px;
  min-height: 23px;

  & + & {
    margin-top: 15px;
  }
`;

const Event = ({ title }: EventCellProps) => {
  return <EventCell>{title}</EventCell>;
};

export default Event;
