/// <reference types="react" />
import { CalendarGridProps } from "../../types";
declare const Layout: ({ events, currentDate, CalendarCellRenderer, todayColorHighlight, outOfMonthColorHighlight }: CalendarGridProps) => JSX.Element;
export default Layout;
