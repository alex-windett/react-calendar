/// <reference types="react" />
import { EventCellProps } from "../../types";
declare const Event: ({ title }: EventCellProps) => JSX.Element;
export default Event;
