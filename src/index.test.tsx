import * as React from "react";
import { create } from "react-test-renderer";
import Calendar from "./";
import moment from "moment";

const events = [
  {
    title: "Two Volcano Sprint",
    startDate: "2020-10-18T05:30+01:00",
    endDate: "2020-10-24T00:00+00:00"
  },
  {
    title: "Two Volcano Sprint",
    startDate: "2020-10-18T05:30+01:00",
    endDate: "2020-10-24T00:00+00:00"
  },
  {
    title: "Two Volcano Sprint",
    startDate: "2020-10-18T05:30+01:00",
    endDate: "2020-10-24T00:00+00:00"
  }
];

describe("Calendar component", () => {
  test("Matches the snapshot with default rendering", () => {
    const Component = create(<Calendar events={events} />);
    expect(Component.toJSON()).toMatchSnapshot();
  });

  test("Matches the snapshot with custom today hightlight", () => {
    const Component = create(<Calendar events={events} todayColorHighlight="pink" />);
    expect(Component.toJSON()).toMatchSnapshot();
  });

  test("Matches the snapshot with custom out of month day hightlight", () => {
    const Component = create(<Calendar events={events} outOfMonthColorHighlight="pink" />);
    expect(Component.toJSON()).toMatchSnapshot();
  });

  test("Matches the snapshot with custom cell render", () => {
    const Component = create(
      <Calendar
        events={events}
        CalendarCellRenderer={props => (
          <div style={{ border: "1px solid lightblue" }}>
            <p>{moment(props.date).format("dddd Do")}</p>
            <p>In View Month {JSON.stringify(props.isCurrentMonth)}</p>
            <p>events:</p>
            {props.events.map((e: any, i: number) => (
              <p key={i}>{e.title}</p>
            ))}
          </div>
        )}
      />
    );
    expect(Component.toJSON()).toMatchSnapshot();
  });

  test("Matches the snapshot with custom month nav render", () => {
    const Component = create(
      <Calendar
        events={events}
        MonthNavRenderer={props => (
          <div>
            <button type="button" onClick={month => console.log(month)}>
              prev
            </button>
            current Month {moment(props.currentDate).format("MMM")}
            <button type="button" onClick={month => console.log(month)}>
              next
            </button>
          </div>
        )}
      />
    );
    expect(Component.toJSON()).toMatchSnapshot();
  });
});
