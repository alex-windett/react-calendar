/// <reference types="react" />
import { CalendarProps } from "./types";
declare const Calendar: (props: CalendarProps) => JSX.Element;
export default Calendar;
