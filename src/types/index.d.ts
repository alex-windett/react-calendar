import { SetStateAction, Dispatch, ReactNode } from "react";

export interface CalendarEvent {
  startDate: string;
  endDate: string;
  title: string;
  start?: Date;
  end?: Date;
}

export interface DateHandlers {
  currentDate: Date | number;
  setCurrentDate: Dispatch<SetStateAction<Date | number>>;
  handleNextMonthClick: () => void;
  handlePrevMonthClick: () => void;
}

export interface DateCellProps {
  isCurrentMonth: boolean;
  events: CalendarEvent[];
  date: number | Date;
  index: number;
  todayColorHighlight?: string;
  outOfMonthColorHighlight?: string;
}

export interface EventCellProps {
  title: string;
  startDate: string;
  endDate: string;
  dayIndex: number;
  calendarDate: Date | number;
}

export interface MonthNavProps extends DateHandlers {
  setCurrentDate: Dispatch<SetStateAction<number | Date>>;
  navMonthFormat?: string;
  navYearsBeforeNow?: number;
  navYearsAfterNow?: number;
}

export interface WeekHeaderProps {
  WeekHeaderRenderer?: (props: WeekHeaderRendererProps) => Element<any> | null;
}

interface CalendarCellRendererProps extends DateCellProps {
  index: number;
}

export interface CalendarGridProps {
  events: CalendarEvent[];
  currentDate: Date | number;
  todayColorHighlight?: string;
  outOfMonthColorHighlight?: string;
  CalendarCellRenderer?: (props: CalendarCellRendererProps) => void;
}

export interface WeekHeaderRendererProps {
  days: string[];
}

export interface MonthNavRenderProps {
  currentDate?: string;
  navMonthFormat?: string;
  handleNextMonthClick: (newMonth: Date) => void;
  handlePrevMonthClick: (newMonth: Date) => void;
  setCurrentDate: () => void;
}

export interface CalendarProps {
  events: CalendarEvent[];
  dateInView?: Date | number;
  navMonthFormat?: string;
  navYearsBeforeNow?: number;
  navYearsAfterNow?: number;
  todayColorHighlight?: string;
  outOfMonthColorHighlight?: string;
  onPrevMonthClick?: (newMonth: Date) => void;
  onNextMonthClick?: (newMonth: Date) => void;
  CalendarCellRenderer?: (props: CalendarCellRendererProps) => void;
  MonthNavRenderer?: (props: any) => void;
  WeekHeaderRenderer?: (props: WeekHeaderRendererProps) => void;
}
