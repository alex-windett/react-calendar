import * as React from "react";

import { addMonths, subMonths } from "date-fns";

import { CalendarProps } from "./types";

import { MonthNav, WeekHeader, CalendarGrid } from "./components";

const Calendar = (props: CalendarProps) => {
  const {
    events,
    dateInView,
    CalendarCellRenderer,
    MonthNavRenderer,
    WeekHeaderRenderer,
    onNextMonthClick,
    onPrevMonthClick,
    navMonthFormat,
    todayColorHighlight,
    outOfMonthColorHighlight,
  } = props;

  const todayHighlight = todayColorHighlight ? todayColorHighlight : "lightblue";
  const outOfMonthyHighlight = outOfMonthColorHighlight ? outOfMonthColorHighlight : "grey";

  const startAt = dateInView || Date.now();

  const [currentDate, setCurrentDate] = React.useState<Date | number>(startAt);

  const handleNextMonthClick = () => {
    const newDate = addMonths(currentDate, 1);
    setCurrentDate(newDate);
    onNextMonthClick && typeof onNextMonthClick === "function" && onNextMonthClick(newDate);
  };

  const handlePrevMonthClick = () => {
    const newDate = subMonths(currentDate, 1);
    setCurrentDate(newDate);
    onPrevMonthClick && typeof onPrevMonthClick === "function" && onPrevMonthClick(newDate);
  };

  return (
    <section>
      {MonthNavRenderer && typeof MonthNavRenderer === "function" ? (
        MonthNavRenderer({
          currentDate,
          setCurrentDate,
        })
      ) : (
        <MonthNav
          handleNextMonthClick={handleNextMonthClick}
          handlePrevMonthClick={handlePrevMonthClick}
          currentDate={currentDate}
          setCurrentDate={setCurrentDate}
          navMonthFormat={navMonthFormat}
        />
      )}
      <WeekHeader WeekHeaderRenderer={WeekHeaderRenderer} />
      <CalendarGrid
        events={events}
        currentDate={currentDate}
        CalendarCellRenderer={CalendarCellRenderer}
        todayColorHighlight={todayHighlight}
        outOfMonthColorHighlight={outOfMonthyHighlight}
      />
    </section>
  );
};

export default Calendar;
